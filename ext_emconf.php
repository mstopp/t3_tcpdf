<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'TCPDF for TYPO3',
    'description' => 'Make TCPDF library (www.tcpdf.org) available within TYPO3',
    'category' => 'misc',
    'version' => '6.2.0',
    'state' => 'stable',
    'author' => 'Michael Stopp',
    'author_email' => 'info@eye.ch',
    'author_company' => 'EYE Communications AG',
    'autoload' => [
        'psr-4' => [
            'EYE\\T3tcpdf\\'=> 'Classes',
        ],
        'classmap' => ['Resources/Private/PHP/tcpdf']
    ],
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
