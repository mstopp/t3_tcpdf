<?php

/**
 * This file is part of the "T3 TCPDF" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2011-2023 Michael Stopp <stopp@eye.ch>
 */

namespace EYE\T3tcpdf\Utility;

use TCPDF2DBarcode;

/**
 * Class to create 2D barcodes as PNG, SVG, HTML
 */
class T3tcpdfBarcodes2d extends TCPDF2DBarcode
{
	/***************************
	 * Sample calls:
	 *
	 * // instantiate barcode class with constructor arguments (see TCPDF documentation for details)
	 * $barcodeObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\EYE\T3tcpdf\Utility\T3tcpdfBarcodes2d::class, 'my data', 'DATAMATRIX');
	 *
	 * // create barcode as HTML
	 * $htmlBarcode = $barcodeObj->getBarcodeHTML(6, 6, 'black', 'mycss');
	 *
	 * // create barcode as PNG image tag
	 * $pngImg = '<img src="data:image/png;base64,'.base64_encode($barcodeObj->getBarcodePngData(6, 6, [0,0,0])).'">';
	 *
	 ***************************/

	/**
	 * Return an HTML representation of barcode.
	 * (extension of parent method to include optional CSS class parameter)
	 *
	 * @param $w (int) Width of a single rectangle element in pixels.
	 * @param $h (int) Height of a single rectangle element in pixels.
	 * @param $color (string) Foreground color for bar elements (background is transparent).
	 * @param $css (string) CSS string; will be set as class attribute 
 	 * @return string HTML code.
 	 * @public
	 */
	public function getBarcodeHTML($w = 10, $h = 10, $color = 'black', $css = '')
	{
		$html = parent::getBarcodeHTML($w, $h, $color);
		if ( $css ) $html = str_replace('<div style=', '<div class="'.$css.'" style=', $html);

		return $html;
	}

}
